<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOfficersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('officers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('officer_id');
            $table->integer('title_id');
            $table->string('firstname_en');
            $table->string('lastname_en');
            $table->string('firstname_th');
            $table->string('lastname_th');
            $table->string('nickname_en')->index();
            $table->string('nickname_th')->index();
            $table->string('position_en')->nullable();
            $table->string('position_th');
            $table->integer('department_id');
            $table->integer('section_id');
            $table->integer('branch_id');
            $table->string('email')->unique();
            $table->string('mobile');
            $table->integer('officer_status_id');
            $table->integer('officer_type_id');
            $table->date('startdate')->nullable();
            $table->date('enddate')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('officers');
    }
}
