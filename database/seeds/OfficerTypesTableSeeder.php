<?php

use Illuminate\Database\Seeder;

class OfficerTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('officer_types')->insert(
        	array([
        		'name_en'		=>	'Permanent',
        		'name_th'		=>	'พนักงานประจำ',
        		'created_at'	=>	new Datetime,
        		'updated_at'	=>	new Datetime
        	],
        	[
        		'name_en'		=>	'Contract',
        		'name_th'		=>	'พนักงานสัญญาจ้าง',
        		'created_at'	=>	new Datetime,
        		'updated_at'	=>	new Datetime
        	],
        	[
        		'name_en'		=>	'Parttime',
        		'name_th'		=>	'พนักงานชั่วคราว',
        		'created_at'	=>	new Datetime,
        		'updated_at'	=>	new Datetime
        	])
        );
    }
}
