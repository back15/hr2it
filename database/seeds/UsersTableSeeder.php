<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
        	array([
        		'username'		=>	'Ppare',
        		'email'			=>	'pare.chompunut@gmail.com',
        		'password'		=>	bcrypt('0894769690'),
        		'user_role_id'	=>	1,
        		'officer_id'	=>	null,
        		'created_at'	=>	new Datetime(),
        		'updated_at'	=>	new Datetime()
        	])
        );
    }
}
