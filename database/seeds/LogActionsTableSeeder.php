<?php

use Illuminate\Database\Seeder;

class LogActionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('log_actions')->insert(
        	array([
        		'name_en'		=>	'Added',
        		'name_th'		=>	'เพิ่ม',
        		'created_at'	=>	new Datetime,
        		'updated_at'	=>	new Datetime
        	],
        	[
        		'name_en'		=>	'Edited',
        		'name_th'		=>	'แก้ไข',
        		'created_at'	=>	new Datetime,
        		'updated_at'	=>	new Datetime
        	],
        	[
        		'name_en'		=>	'Deleted',
        		'name_th'		=>	'ลบ',
        		'created_at'	=>	new Datetime,
        		'updated_at'	=>	new Datetime
        	])
        );
    }
}
