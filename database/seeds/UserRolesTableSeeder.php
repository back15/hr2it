<?php

use Illuminate\Database\Seeder;

class UserRolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_roles')->insert(
        	array([
        		'name_en'	=>	'Administrator',
        		'name_th'	=>	'ผู้ดูแลระบบ',
        		'created_at'	=>	new Datetime(),
        		'updated_at'	=>	new Datetime()
        	],
        	[
        		'name_en'	=>	'HR',
        		'name_th'	=>	'บริหารทรัพยากรบุคคล',
        		'created_at'	=>	new Datetime(),
        		'updated_at'	=>	new Datetime()
        	])
        );
    }
}
