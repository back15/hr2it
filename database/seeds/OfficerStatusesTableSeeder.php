<?php

use Illuminate\Database\Seeder;

class OfficerStatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('officer_statuses')->insert(
        	array([
        		'name_en'		=>	'Active',
        		'name_th'		=>	'ทำงาน',
        		'created_at'	=>	new Datetime,
        		'updated_at'	=>	new Datetime
        	],
        	[
        		'name_en'		=>	'Resign',
        		'name_th'		=>	'ลาออก',
        		'created_at'	=>	new Datetime,
        		'updated_at'	=>	new Datetime
        	])
        );
    }
}
