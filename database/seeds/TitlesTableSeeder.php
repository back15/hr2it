<?php

use Illuminate\Database\Seeder;

class TitlesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('titles')->insert(
        	array([
        		'name_en'		=>	'Mr.',
        		'name_th'		=>	'นาย',
        		'created_at'	=>	new Datetime,
        		'updated_at'	=>	new Datetime
        	],
        	[
        		'name_en'		=>	'Mrs.',
        		'name_th'		=>	'นาง',
        		'created_at'	=>	new Datetime,
        		'updated_at'	=>	new Datetime
        	],
        	[
        		'name_en'		=>	'Ms.',
        		'name_th'		=>	'นางสาว',
        		'created_at'	=>	new Datetime,
        		'updated_at'	=>	new Datetime
        	])
        );
    }
}
