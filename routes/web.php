<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/checkrole', 'MainController@checkRole');

Route::group(['namespace' => 'frontend'], function() {
	Route::get('/', function () {
    	return view('frontend.email');
	});
	Route::get('/lang/{locale}', function($locale) {
		Session::put('locale', $locale);
		return redirect()->back();
	});
	Route::get('/search', [
		'as'	=>	'frontend.email.search',
		'uses'	=>	'MainController@searchEmail'
	]);
	Route::get('/mailgroup', [
		'as'	=>	'frontend.mailgroup',
		'uses'	=>	'MainController@listMailGroup'
	]);
	Route::get('/phone', [
		'as'	=>	'frontend.phone',
		'uses'	=>	'MainController@listPhone'
	]);
	Route::get('/mobile', [
		'as'	=>	'frontend.mobile',
		'uses'	=>	'MainController@listMobile'
	]);
	Route::get('/show/{mail_group_id}', [
		'as'	=>	'frontend.showmember',
		'uses'	=>	'MainController@showMember'
	]);
});

Route::group(['namespace' => 'Backend', 'middleware' => 'auth'], function() {
	Route::get('/home', [
		'as'	=>	'backend.home',
		'uses'	=>	'MainController@index'
	]);

	Route::group(['prefix' => 'user'], function() {
		Route::get('/list', [
			'as'	=>	'backend.user.list',
			'uses'	=>	'UserController@index'
		]);
		Route::get('/create', [
			'as'	=>	'backend.user.create.get',
			'uses'	=>	'UserController@getCreate'
		]);
		Route::post('/create', [
			'as'	=>	'backend.user.create.post',
			'uses'	=>	'UserController@postCreate'
		]);
		Route::get('/edit/{user_id}', [
			'as'	=>	'backend.user.edit.get',
			'uses'	=>	'UserController@getEdit'
		]);
		Route::post('/edit/{user_id}', [
			'as'	=>	'backend.user.edit.post',
			'uses'	=>	'UserController@postEdit'
		]);
		Route::delete('/delete/{user_id}', [
			'as'	=>	'backend.user.delete',
			'uses'	=>	'UserController@delete'
		]);
	});

	Route::group(['prefix' => 'title'], function() {
		Route::get('/list', [
			'as'	=>	'backend.title.list',
			'uses'	=>	'TitleController@index'
		]);
		Route::get('/search', [
			'as'	=>	'backend.title.search',
			'uses'	=>	'TitleController@search'
		]);
		Route::get('/create', [
			'as'	=>	'backend.title.create.get',
			'uses'	=>	'TitleController@getCreate'
		]);
		Route::post('/create', [
			'as'	=>	'backend.title.create.post',
			'uses'	=>	'TitleController@postCreate'
		]);
		Route::get('/edit/{title_id}', [
			'as'	=>	'backend.title.edit.get',
			'uses'	=>	'TitleController@getEdit'
		]);
		Route::post('/edit/{title_id}', [
			'as'	=>	'backend.title.edit.post',
			'uses'	=>	'TitleController@postEdit'
		]);
		Route::delete('/delete/{title_id}', [
			'as'	=>	'backend.title.delete',
			'uses'	=>	'TitleController@delete'
		]);
	});

	Route::group(['prefix' => 'setting'], function() {
		Route::get('/changepassword', [
			'as'	=>	'backend.setting.changepassword.get',
			'uses'	=>	'SettingController@getChangePassword'
		]);
		Route::post('/changepassword', [
			'as'	=>	'backend.setting.changepassword.post',
			'uses'	=>	'SettingController@postChangePassword'
		]);
		Route::get('/changelang', [
			'as'	=>	'backend.setting.changelang.get',
			'uses'	=>	'SettingController@getChangeLang'
		]);
		Route::post('/changelang', [
			'as'	=>	'backend.setting.changelang.post',
			'uses'	=>	'SettingController@postChangeLang'
		]);
	});

	Route::group(['prefix' => 'section'], function() {
		Route::get('/list', [
			'as'	=>	'backend.section.list',
			'uses'	=>	'SectionController@index'
		]);
		Route::get('/search', [
			'as'	=>	'backend.section.search',
			'uses'	=>	'SectionController@search'
		]);
		Route::get('/create', [
			'as'	=>	'backend.section.create.get',
			'uses'	=>	'SectionController@getCreate'
		]);
		Route::post('/create', [
			'as'	=>	'backend.section.create.post',
			'uses'	=>	'SectionController@postCreate'
		]);
		Route::get('/edit/{section_id}', [
			'as'	=>	'backend.section.edit.get',
			'uses'	=>	'SectionController@getEdit'
		]);
		Route::post('/edit/{section_id}', [
			'as'	=>	'backend.section.edit.post',
			'uses'	=>	'SectionController@postEdit'
		]);
		Route::delete('/delete/{section_id}', [
			'as'	=>	'backend.section.delete',
			'uses'	=>	'SectionController@delete'
		]);
	});

	Route::group(['prefix' => 'phone'], function() {
		Route::get('/list', [
			'as'	=>	'backend.phone.list',
			'uses'	=>	'PhoneController@index'
		]);
		Route::get('/create', [
			'as'	=>	'backend.phone.create.get',
			'uses'	=>	'PhoneController@getCreate'
		]);
		Route::post('/create', [
			'as'	=>	'backend.phone.create.post',
			'uses'	=>	'PhoneController@postCreate'
		]);
		Route::get('/edit/{phone_id}', [
			'as'	=>	'backend.phone.edit.get',
			'uses'	=>	'PhoneController@getEdit'
		]);
		Route::post('/edit/{phone_id}', [
			'as'	=>	'backend.phone.edit.post',
			'uses'	=>	'PhoneController@postEdit'
		]);
		Route::delete('/delete/{phone_id}', [
			'as'	=>	'backend.phone.delete',
			'uses'	=>	'PhoneController@delete'
		]);
	});

	Route::group(['prefix' => 'officer'], function() {
		Route::get('/list', [
			'as'	=>	'backend.officer.list',
			'uses'	=>	'OfficerController@index'
		]);
		Route::get('/search', [
			'as'	=>	'backend.officer.search',
			'uses'	=>	'OfficerController@search'
		]);
		Route::get('/show/{officer_id}', [
			'as'	=>	'backend.officer.show',
			'uses'	=>	'OfficerController@show'
		]);
		Route::get('/create', [
			'as'	=>	'backend.officer.create.get',
			'uses'	=>	'OfficerController@getCreate'
		]);
		Route::post('/create', [
			'as'	=>	'backend.officer.create.post',
			'uses'	=>	'OfficerController@postCreate'
		]);
		Route::get('/edit/{officer_id}', [
			'as'	=>	'backend.officer.edit.get',
			'uses'	=>	'OfficerController@getEdit'
		]);
		Route::post('/edit/{officer_id}', [
			'as'	=>	'backend.officer.edit.post',
			'uses'	=>	'OfficerController@postEdit'
		]);
		Route::get('/changestatus/{officer_id}', [
			'as'	=>	'backend.officer.changestatus',
			'uses'	=>	'OfficerController@changeStatus'
		]);
	});

	Route::group(['prefix' => 'notice'], function() {
		Route::get('/list', [
			'as'	=>	'backend.notice.list',
			'uses'	=>	'NoticeController@index'
		]);
		Route::get('/new', [
			'as'	=>	'backend.notice.new.get',
			'uses'	=>	'NoticeController@getNoticeNew'
		]);
		Route::post('/new', [
			'as'	=>	'backend.notice.new.post',
			'uses'	=>	'NoticeController@postNoticeNew'
		]);
		Route::get('/resign', [
			'as'	=>	'backend.notice.resign.get',
			'uses'	=>	'NoticeController@getNoticeResign'
		]);
		Route::post('/resign', [
			'as'	=>	'backend.notice.resign.post',
			'uses'	=>	'NoticeController@postNoticeResign'
		]);
	});

	Route::group(['prefix' => 'mobile'], function() {
		Route::get('/list', [
			'as'	=>	'backend.mobile.list',
			'uses'	=>	'MobileController@index'
		]);
		Route::get('/search', [
			'as'	=>	'backend.mobile.search',
			'uses'	=>	'MobileController@search'
		]);
		Route::get('/create', [
			'as'	=>	'backend.mobile.create.get',
			'uses'	=>	'MobileController@getCreate'
		]);
		Route::post('/create', [
			'as'	=>	'backend.mobile.create.post',
			'uses'	=>	'MobileController@postCreate'
		]);
		Route::get('/edit/{mobile_id}', [
			'as'	=>	'backend.mobile.edit.get',
			'uses'	=>	'MobileController@getEdit'
		]);
		Route::post('/edit/{mobile_id}', [
			'as'	=>	'backend.mobile.edit.post',
			'uses'	=>	'MobileController@postEdit'
		]);
		Route::delete('/delete/{mobile_id}', [
			'as'	=>	'backend.mobile.delete',
			'uses'	=>	'MobileController@delete'
		]);
	});

	Route::group(['prefix' => 'mailgroup'], function() {
		Route::get('/list', [
			'as'	=>	'backend.mailgroup.list',
			'uses'	=>	'MailGroupController@index'
		]);
		Route::get('/search', [
			'as'	=>	'backend.mailgroup.search',
			'uses'	=>	'MailGroupController@search'
		]);
		Route::get('/create', [
			'as'	=>	'backend.mailgroup.create.get',
			'uses'	=>	'MailGroupController@getCreate'
		]);
		Route::post('/create', [
			'as'	=>	'backend.mailgroup.create.post',
			'uses'	=>	'MailGroupController@postCreate'
		]);
		Route::get('/edit/{mail_group_id}', [
			'as'	=>	'backend.mailgroup.edit.get',
			'uses'	=>	'MailGroupController@getEdit'
		]);
		Route::post('/edit/{mail_group_id}', [
			'as'	=>	'backend.mailgroup.edit.post',
			'uses'	=>	'MailGroupController@postEdit'
		]);
		Route::delete('/delete/{mail_group_id}', [
			'as'	=>	'backend.mailgroup.delete',
			'uses'	=>	'MailGroupController@delete'
		]);
		Route::get('/addmember/{mail_group_id}', [
			'as'	=>	'backend.mailgroup.addmember.get',
			'uses'	=>	'MailGroupController@getAddMember'
		]);
		Route::post('/addmember/{mail_group_id}', [
			'as'	=>	'backend.mailgroup.addmember.post',
			'uses'	=>	'MailGroupController@postAddMember'
		]);
		Route::get('/showmember/{mail_group_id}', [
			'as'	=>	'backend.mailgroup.showmember',
			'uses'	=>	'MailGroupController@showMember'
		]);
		Route::delete('/deletemember/{officer_in_mail_group_id}', [
			'as'	=>	'backend.mailgroup.deletemember',
			'uses'	=>	'MailGroupController@deleteMember'
		]);
	});

	Route::group(['prefix' => 'extension'], function() {
		Route::get('/list', [
			'as'	=>	'backend.extension.list',
			'uses'	=>	'ExtensionController@index'
		]);
		Route::get('/search', [
			'as'	=>	'backend.extension.search',
			'uses'	=>	'ExtensionController@search'
		]);
		Route::get('/create', [
			'as'	=>	'backend.extension.create.get',
			'uses'	=>	'ExtensionController@getCreate'
		]);
		Route::post('/create', [
			'as'	=>	'backend.extension.create.post',
			'uses'	=>	'ExtensionController@postCreate'
		]);
		Route::get('/edit/{extension_id}', [
			'as'	=>	'backend.extension.edit.get',
			'uses'	=>	'ExtensionController@getEdit'
		]);
		Route::post('/edit/{extension_id}', [
			'as'	=>	'backend.extension.edit.post',
			'uses'	=>	'ExtensionController@postEdit'
		]);
		Route::delete('/delete/{extension_id}', [
			'as'	=>	'backend.extension.delete',
			'uses'	=>	'ExtensionController@delete'
		]);
	});

	Route::group(['prefix' => 'department'], function() {
		Route::get('/list', [
			'as'	=>	'backend.department.list',
			'uses'	=>	'DepartmentController@index'
		]);
		Route::get('/search', [
			'as'	=>	'backend.department.search',
			'uses'	=>	'DepartmentController@search'
		]);
		Route::get('/create', [
			'as'	=>	'backend.department.create.get',
			'uses'	=>	'DepartmentController@getCreate'
		]);
		Route::post('/create', [
			'as'	=>	'backend.department.create.post',
			'uses'	=>	'DepartmentController@postCreate'
		]);
		Route::get('/edit/{department_id}', [
			'as'	=>	'backend.department.edit.get',
			'uses'	=>	'DepartmentController@getEdit'
		]);
		Route::post('/edit/{department_id}', [
			'as'	=>	'backend.department.edit.post',
			'uses'	=>	'DepartmentController@postEdit'
		]);
		Route::delete('/delete/{department_id}', [
			'as'	=>	'backend.department.delete',
			'uses'	=>	'DepartmentController@delete'
		]);
	});

	Route::group(['prefix' => 'branch'], function() {
		Route::get('/list', [
			'as'	=>	'backend.branch.list',
			'uses'	=>	'BranchController@index'
		]);
		Route::get('/search', [
			'as'	=>	'backend.branch.search',
			'uses'	=>	'BranchController@search'
		]);
		Route::get('/create', [
			'as'	=>	'backend.branch.create.get',
			'uses'	=>	'BranchController@getCreate'
		]);
		Route::post('/create', [
			'as'	=>	'backend.branch.create.post',
			'uses'	=>	'BranchController@postCreate'
		]);
		Route::get('/edit/{branch_id}', [
			'as'	=>	'backend.branch.edit.get',
			'uses'	=>	'BranchController@getEdit'
		]);
		Route::post('/edit/{branch_id}', [
			'as'	=>	'backend.branch.edit.post',
			'uses'	=>	'BranchController@postEdit'
		]);
		Route::delete('/delete/{branch_id}', [
			'as'	=>	'backend.branch.delete',
			'uses'	=>	'BranchController@delete'
		]);
	});
});

