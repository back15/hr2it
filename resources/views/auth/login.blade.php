@extends('frontend.layouts.app')

@section('content')
<div class="ui center aligned inverted container segment" id="login-page">
    <h1 class="ui header">{{ trans('frontend.login') }}</h1>
    <div class="ui hidden divider"></div>
    <div class="ui grid">
        <div class="row">
            <div class="five wide column"></div>
            <div class="six wide column">
                <form class="ui form" method="POST" action="{{ route('login') }}">
                    {{ csrf_field() }}
                    <div class="field {{ $errors->has('username') ? 'has-error' : '' }}">
                        <div class="ui labeled input">
                            <div class="ui orange label">{{ trans('frontend.login_form.username') }}</div>
                            <input type="text" name="username" value="{{ old('username') }}" placeholder="{{ trans('frontend.login_form.username') }}">
                        </div>
                    </div>
                    <div class="field {{ $errors->has('password') ? 'has-error' : '' }}">
                        <div class="ui labeled input">
                            <div class="ui orange label">{{ trans('frontend.login_form.password') }}</div>
                            <input type="password" name="password" placeholder="{{ trans('frontend.login_form.password') }}">
                        </div>
                    </div>
                    @if ($errors->has('username'))
                        <div class="ui red mini message">{{ $errors->first('username') }}</div>
                    @endif
                    @if ($errors->has('password'))
                        <div class="ui red mini message">{{ $errors->first('password') }}</div>
                    @endif
                    <div class="field">
                        <div class="ui checkbox">
                            <input type="checkbox" name="remember" id="remember" value="{{ old('remember') ? 'checked' : '' }}">
                            <label for="remember">{{ trans('frontend.login_form.remember') }}</label>
                        </div>
                    </div>
                    <div class="field">
                        <button class="ui orange inverted button" type="submit">
                            {{ trans('frontend.login_form.submit') }}
                        </button>
                    </div>
                    <div class="field">
                        <a href="{{ route('password.request') }}">
                            {{ trans('frontend.login_form.forget') }}
                        </a>   
                    </div>
                </form>
            </div>
            <div class="five wide column"></div>
        </div>
    </div>
</div>
@endsection
