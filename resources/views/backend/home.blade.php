@extends('backend.layouts.app')

@section('content')

			<div class="active section">{{ trans('layout.menu.home') }}</div>
		</div>
	</div>
</div>

<div class="pusher">
	<div class="ui bottom fixed container">
		<div class="ui main grid">
			<div class="ui dimmer">
				<div class="ui text loader">Loading</div>
			</div>
			<div class="row">
				<div class="column">
					
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
