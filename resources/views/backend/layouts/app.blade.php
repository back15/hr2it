<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
	<title>{{ trans('layout.title') . '|' . trans('layout.bac') }}</title>
	<link rel="icon" href="{{ asset('images/logo.png') }}">

	<!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
	
	<link rel="stylesheet" href="{{ asset('css/semantic.min.css') }}">
	<link rel="stylesheet" href="{{ asset('css/main.css') }}">
	<link rel="stylesheet" href="{{ asset('css/calendar.min.css') }}">

	<script src="{{ asset('js/jquery.min.js') }}"></script>
	<script src="{{ asset('js/semantic.min.js') }}"></script>
    <script src="{{ asset('js/calendar.min.js') }}"></script>
</head>
<body>
	{{ Session::put('locale', Auth::user()->lang) }}

	<!-- Sidebar -->
	<div class="ui left fixed vertical sidebar menu visible">
		<div class="top item">
			<div class="ui hidden divider"></div>
			<img class="ui centered image" src="{{ asset('images/logo.svg') }}">
			<h4 class="ui centered orange header">
				{{ trans('layout.bac') }}
			</h4>
			<div class="ui hidden divider"></div>
		</div>
		<a class="menu item {{ Route::current()->uri() == 'home' ? 'active' : '' }}" href="{{ route('backend.home') }}">
			<i class="home icon"></i>
			{{ trans('layout.menu.home') }}
		</a>

		<!-- HR Notice Menu -->
		@if (Auth::user()->user_role_id == 2)
			<div class="ui accordion">
				<div class="item">
					<a class="title">
						<i class="announcement icon"></i>
						{{ trans('layout.menu.notice') }}
						<i class="dropdown icon"></i>
					</a>
					<div class="content">
						<a class="item" href="">
							{{ trans('layout.submenu.notice.list') }}
							<i class="list icon"></i>
						</a>
						<a class="item {{ Route::getCurrentRoute()->getPrefix() == '/officer' ? 'active' : '' }}" href="">
							{{ trans('layout.submenu.notice.new') }}
							<i class="add user icon"></i>
						</a>
						<a class="item {{ Route::getCurrentRoute()->getPrefix() == '/officer' ? 'active' : '' }}" href="">
							{{ trans('layout.submenu.notice.resign') }}
							<i class="remove user icon"></i>
						</a>
					</div>
				</div>
			</div>
		@endif
		<div class="ui accordion">
			<div class="item">
				<div class="title">
					<i class="users icon"></i>
					{{ trans('layout.menu.officer') }}
					<i class="dropdown icon"></i>
				</div>
				<div class="content">
					<a class="item {{ Route::getCurrentRoute()->getPrefix() == '/officer' ? 'active' : '' }}" href="">
						<i class="list icon"></i>
						{{ trans('layout.submenu.officer.officerlist') }}
					</a>
					<a class="item {{ Route::getCurrentRoute()->getPrefix() == '/title' ? 'active' : '' }}" href="">
						<i class="list icon"></i>
						{{ trans('layout.submenu.officer.titlelist') }}
					</a>
				</div>
			</div>
			<div class="item">
				<div class="title">
					<i class="building icon"></i>
					{{ trans('layout.menu.company') }}
					<i class="dropdown icon"></i>
				</div>
				<div class="content">
					<a class="item" href="">
						<i class="list icon"></i>
						{{ trans('layout.submenu.company.departmentlist') }}
					</a>
					<a class="item {{ Route::getCurrentRoute()->getPrefix() == '/officer' ? 'active' : '' }}" href="">
						<i class="list icon"></i>
						{{ trans('layout.submenu.company.sectionlist') }}
					</a>
					<a class="item {{ Route::getCurrentRoute()->getPrefix() == '/officer' ? 'active' : '' }}" href="">
						<i class="list icon"></i>
						{{ trans('layout.submenu.company.branchlist') }}
					</a>
				</div>
			</div>
		</div>
		<a class="menu item" href="">
			<i class="envelope icon"></i>
			{{ trans('layout.menu.mailgroup') }}
		</a>
		<a class="menu item" href="">
			<i class="mobile icon"></i>
			{{ trans('layout.menu.mobile') }}
		</a>
		<div class="ui accordion">
			<div class="item">
				<div class="title">
					<i class="phone icon"></i>
					{{ trans('layout.menu.ext') }}
					<i class="dropdown icon"></i>
				</div>
				<div class="content">
					<a class="item" href="">
						<i class="list icon"></i>
						{{ trans('layout.submenu.ext.phonelist') }}
					</a>
					<a class="item" href="">
						<i class="list icon"></i>
						{{ trans('layout.submenu.ext.extlist') }}
					</a>
				</div>
			</div>
			@if (Auth::user()->user_role_id == 1)
				<a class="menu item" href="">
					<i class="user icon"></i>
					{{ trans('layout.menu.user') }}
				</a>
			@endif
			<div class="item {{ Route::getCurrentRoute()->getPrefix() == '/setting' ? 'active' : '' }}">
				<div class="title {{ Route::getCurrentRoute()->getPrefix() == '/setting' ? 'active' : '' }}">
					<i class="settings icon"></i>
					{{ trans('layout.menu.setting') }} 
					<i class="dropdown icon"></i>
				</div>
				<div class="content {{ Route::getCurrentRoute()->getPrefix() == '/setting' ? 'active' : '' }}">
					<a class="item {{ Route::current()->uri() == 'setting/changepassword' ? 'active' : '' }}" href="{{ route('backend.setting.changepassword.get') }}">
						<i class="lock icon"></i>
						{{ trans('layout.submenu.setting.changepassword') }}
					</a>
					<a class="item {{ Route::current()->uri() == 'setting/changelang' ? 'active' : '' }}" href="{{ route('backend.setting.changelang.get') }}">
						<i class="flag icon"></i>
						{{ trans('layout.submenu.setting.changelang') }}
					</a>
				</div>
			</div>
		</div>
	</div>

	<!-- Top Menu -->
	<div class="ui top fixed stackable menu">
		<a class="item" id="toggle">
			<i class="sidebar icon"></i>
		</a>
		<div class="item">
			<h4 class="ui inverted header">
				{{ trans('layout.title') }}
			</h4>
		</div>
		<div class="ui right item">
			<h4 class="ui inverted header">
				@if (Auth::user()->lang == 'en')
					Hi, {{ Auth::user()->username }}
				@elseif (Auth::user()->lang == 'th')
					สวัสดี {{ Auth::user()->username }}
				@endif
			</h4>
		</div>
		<div class="item">
			<h5 class="ui header">
				<a class="ui mini button" href="{{ route('logout') }}">
					{{ trans('layout.menu.logout') }}
				</a>
			</h5>
		</div>
	</div>

	<div class="ui stackable second fixed menu">
		<div class="item">
			<div class="ui breadcrumb">
			
	@yield('content')
	<script type="text/javascript">
		$('.ui.accordion')
  			.accordion()
		;
		$('#toggle').click(function() {
	$('.ui.sidebar')
	.sidebar({
		dimPage: false,
		closable: false
	})
	.sidebar('toggle');
});
	</script>
</body>
</html>