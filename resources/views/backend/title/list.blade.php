@extends('backend.layouts.app')

@section('content')

			<a class="section" href="{{ route('backend.home') }}">{{ trans('layout.menu.home') }}</a>
			<i class="right angle icon divider"></i>
			<div class="section">{{ trans('layout.menu.officer') }}</div>
			<i class="right angle icon divider"></i>
			<div class="active section">{{ trans('layout.submenu.officer.titlelist') }}</div>
		</div>
	</div>
</div>

<div class="pusher">
	<div class="ui bottom fixed container">
		<div class="ui main grid">
			<div class="ui dimmer">
				<div class="ui text loader">Loading</div>
			</div>
			<div class="row">
				<div class="column">
					
				</div>
			</div>
		</div>
	</div>
</div>
@endsection