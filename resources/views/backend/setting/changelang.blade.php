@extends('backend.layouts.app')

@section('content')

			<a class="section" href="{{ route('backend.home') }}">{{ trans('layout.menu.home') }}</a>
			<i class="right angle icon divider"></i>
			<div class="section">{{ trans('layout.menu.setting') }}</div>
			<i class="right angle icon divider"></i>
			<div class="active section">{{ trans('layout.submenu.setting.changelang') }}</div>
		</div>
	</div>
</div>

<div class="pusher">
	<div class="ui bottom fixed container">
		<div class="ui center aligned main grid">
			<div class="ui dimmer">
				<div class="ui text loader">Loading</div>
			</div>
			<div class="row">
				<div class="ui two column grid">
					<div class="column">
						<div class="ui padded evergreen segment">
							<h3 class="ui centered inverted header">
								{{ trans('layout.submenu.setting.changelang') }}
							</h3>
							<form class="ui form" method="POST" action="{{ route('backend.setting.changelang.post') }}">
								{{ csrf_field() }}
								<div class="ui very padded segment">
									<div class="ui column centered grid">
										<div class="inline fields">
											<div class="field">
												<div class="ui checked checkbox">
													<input type="radio" name="lang" value="0" {{ Auth::user()->lang == 'en' ? 'checked=""' : '' }}>
													<label>
														<i class="us flag"></i>
														{{ trans('layout.lang.en') }}
													</label>
												</div>
											</div>
											<div class="field">
												<div class="ui checked checkbox">
													<input type="radio" name="lang" value="1" {{ Auth::user()->lang == 'th' ? 'checked=""' : '' }}>
													<label>
														<i class="th flag"></i>
														{{ trans('layout.lang.th') }}
													</label>
												</div>
											</div>
										</div>
									</div>
									<div class="ui hidden divider"></div>
									<div class="field">
										<button class="ui button" type="submit">
											{{ trans('layout.button.submit') }}
										</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection