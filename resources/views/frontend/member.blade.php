@extends('frontend.layouts.app')

@section('content')

<div class="ui center aligned inverted container segment">
	@if ($_SESSION['locale'] == 'en')
		<h1 class="ui header">{{ trans('frontend.member') }}</h1>
	@else
		<h1 class="ui header">{{ trans('frontend.member') }}</h1>
	@endif
	<div class="ui hidden divider"></div>
	<table class="ui striped celled table">
		<thead>
			<tr class="ui center aligned">
				<th>#</th>
				<th>{{ trans('frontend.table.name') }}</th>
				<th>{{ trans('frontend.table.nickname') }}</th>
				<th>{{ trans('frontend.table.email') }}</th>
			</tr>
		</thead>
		<tbody>
			@forelse ($members as $index => $member)
				<tr>
					<td class="ui center aligned">{{ ++index }}</td>
					@if ($_SESSION['locale'] == 'en')
						<td></td>
						<td></td>
					@else
						<td></td>
						<td></td>
					@endif
					<td></td>
				</tr>
			@empty
				<tr class="ui center aligned">
					<td colspan="4">{{ trans('frontend.table.data_not_found') }}</td>
				</tr>
			@endforelse
		</tbody>
	</table>
</div>

@endsection