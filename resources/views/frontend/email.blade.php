@extends('frontend.layouts.app')

@section('content')

<div class="ui center aligned inverted container segment">
	<h1 class="ui header">{{ trans('frontend.title') }}</h1>
	<div class="ui hidden divider"></div>
	<form id="searchbox" class="ui large form" method="GET" action="{{ route('frontend.email.search') }}">
		{{ csrf_field() }}
		<div class="three fields">
			<div class="three wide field"></div>
			<div class="ten wide field">
				<div class="ui action input">
					<input type="text" name="keyword" placeholder="{{ trans('frontend.search_box') }}" value="{{ old('keyword') }}">
					<select name="category" class="ui selection dropdown">
						<option value="all">{{ trans('frontend.search_option.all') }}</option>
						<option value="nickname">
							{{ trans('frontend.search_option.nickname') }}
						</option>
						<option value="name">{{ trans('frontend.search_option.name') }}</option>
						<option value="department">
							{{ trans('frontend.search_option.department') }}
						</option>
						<option value="section">{{ trans('frontend.search_option.section') }}</option>
					</select>
					<button class="ui submit orange large button" type="submit">
						{{ trans('frontend.search') }}
					</button>
				</div>
			</div>
			<div class="three wide field"></div>
		</div>
	</form>
	<div class="ui hidden divider"></div>
	<table class="ui striped celled center aligned table">
		<thead>
			<tr>
				<th>#</th>
				<th>{{ trans('frontend.table.name') }}</th>
				<th>{{ trans('frontend.table.nickname') }}</th>
				<th>{{ trans('frontend.table.department') }}</th>
				<th>{{ trans('frontend.table.email') }}</th>
			</tr>
		</thead>
		<tbody>
			@if (isset($_GET['keyword']))
				@forelse ($officers as $index => $officer)
					@if ($_SESSION['locale'] == 'en')
						<tr>
							<td>{{ ++$index }}</td>
							<td>{{ $officer['firstname_en'] . ' ' . $officer['lastname_en'] }}</td>
							<td>{{ $officer['nickname_en'] }}</td>
							<td>{{ $officer->getDepartment['name_en'] . '(' . $officer->getDepartment['code'] . ')' }}</td>
							<td>{{ $officer['email'] }}</td>
						</tr>
					@else
						<tr>
							<td>{{ ++$index }}</td>
							<td>{{ $officer['firstname_th'] . ' ' . $officer['lastname_th'] }}</td>
							<td>{{ $officer['nickname_th'] }}</td>
							<td>{{ $officer->getDepartment['name_th'] . '(' . $officer->getDepartment['code'] . ')' }}</td>
							<td>{{ $officer['email'] }}</td>
						</tr>
					@endif
				@empty
					<tr>
						<td colspan="5">{{ trans('frontend.table.data_not_found') }}</td>
					</tr>
				@endforelse
			@else
				<tr>
					<td colspan="5">{{ trans('frontend.table.search_line') }}</td>
				</tr>
			@endif
		</tbody>
	</table>
</div>

@endsection