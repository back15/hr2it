<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>{{ trans('frontend.title') }} | {{ trans('frontend.bac') }}</title>
	<link rel="icon" href="{{ url('images/logo.png') }}">

	<!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

	<link rel="stylesheet" type="text/css" href="{{ asset('css/semantic.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">

	<script type="text/javascript" src="{{ asset('js/jquery.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/semantic.min.js') }}"></script>
</head>
<body>
	<div class="ui inverted centered segment">
		<div class="ui page grid">
			<div class="column">
				<div class="ui secondary pointing menu">
					<a href="http://www.bangkokflying.com">
						<img class="ui tiny image" src="{{ url('images/logo.png') }}">
					</a>
					<a class="item {{ Route::current()->uri() == '/' ? 'active' : '' }}" href="{{ url('/') }}">
						{{ trans('frontend.title') }}
					</a>
					<a class="item {{ Route::current()->uri() == 'mailgroup' ? 'active' : '' }}" href="{{ url('/mailgroup') }}">
						{{ trans('frontend.mailgroup') }}
					</a>
					<a class="item {{ Route::current()->uri() == 'phone' ? 'active' : '' }}" href="{{ url('/phone') }}">
						{{ trans('frontend.extension') }}
					</a>
					<a class="item {{ Route::current()->uri() == 'mobile' ? 'active' : '' }}" href="{{ url('/mobile') }}">
						{{ trans('frontend.mobile') }}
					</a>
					<div class="right menu">
						<a class="item {{ Route::current()->uri() == 'login' ? 'active' : '' }}" href="{{ url('/login') }}">
							{{ trans('frontend.login') }}
						</a>
						<div class="ui dropdown item">
      						{{ trans('frontend.choose_lang') }} <i class="dropdown icon"></i>
      						<div class="menu">
        						<a class="item" href="{{ url('/lang/en') }}">English</a>
        						<a class="item" href="{{ url('/lang/th') }}">ภาษาไทย</a>
      						</div>
    					</div>				
					</div>
				</div>	
			</div>
		</div>
	</div>
	@yield('content')
	<script type="text/javascript">
		$('.ui.dropdown')
			.dropdown()
		;
	</script>
</body>
</html>