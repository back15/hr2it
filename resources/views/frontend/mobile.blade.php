@extends('frontend.layouts.app')

@section('content')

<div class="ui center aligned inverted container segment">
	<h1 class="ui header">{{ trans('frontend.mobile') }}</h1>
	<div class="ui hidden divider"></div>
	<table class="ui striped celled table">
		<thead>
			<tr class="ui center aligned">
				<th>#</th>
				<th>{{ trans('frontend.table.group_name') }}</th>
				<th>{{ trans('frontend.table.number') }}</th>
			</tr>
		</thead>
		<tbody>
			@forelse ($mobiles as $index => $mobile)
				<tr>
					<td>{{ ++$index }}</td>
					@if ($_SESSION['locale'] == 'en')
						<td></td>
					@else
						<td></td>
					@endif
					<td></td>
				</tr>
			@empty
				<tr>
					<td colspan="3">{{ trans('frontend.table.data_not_found') }}</td>
				</tr>
			@endforelse
		</tbody>
	</table>
</div>

@endsection