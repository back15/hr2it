<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'พาสเวิร์ดต้องมีความยาวไม่ต่ำกว่า 8 ตัวอักษร',
    'reset' => 'รีเซ็ทรหัสผ่านของคุณเรียบร้อยแล้ว',
    'sent' => 'ระบบได้ทำการจัดส่งอีเมลล์สำหรับรีเซ็ทรหัสผ่านให้คุณแล้ว',
    'token' => 'This password reset token is invalid.',
    'user' => "ไม่พบผู้ใช้งาน",

];
