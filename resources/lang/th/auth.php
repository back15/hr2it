<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'ชื่อผู้ใช้หรือรหัสผ่านของคุณไม่ถูกต้อง',
    'throttle' => 'คุณใส่รหัสผิดมากเกินไป โปรดลองใหม่อีกครั้งภายใน :seconds วินาที',

];
