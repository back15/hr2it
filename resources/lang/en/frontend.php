<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Top Menu Language Lines
    |--------------------------------------------------------------------------
    */

    'title'         =>  'E-mail Directories',
    'bac'           =>  'Bangkok Avation Center',
    'home'          =>  'Home',
    'login'         =>  'Login',
    'email'         =>  'E-mail Address',
    'mailgroup'     =>  'Mail Group',
    'extension'     =>  'Phone & Extension',
    'mobile'        =>  'Mobile Group',
    'member'        =>  'Group Member',
    'search'        =>  'Search',
    'table'    =>  [
        'name'          =>  'Fullname',
        'nickname'      =>  'Nickname',
        'department'    =>  'Dapartment',
        'branch'        =>  'Branch',
        'number'        =>  'Number',
        'group_name'    =>  'Group Name',
        'email'         =>  'E-mail',
        'data_not_found'    =>  'Data Not Found',
        'search_line'       =>  'Please enter your keyword then click search.',
    ],
    'group'         =>  'Group',
    'choose_lang'   =>  'Language',
    'search_box'    =>  'Enter firstname, lastname, nickname or department',
    'search_option' =>  [
        'all'       =>  'All',
        'name'      =>  'Fullname',
        'nickname'  =>  'Nickname',
        'department'    =>  'Department',
        'section'   =>  'Section',
    ],
    'login_form'    =>  [
        'username'  =>  'Username',
        'password'  =>  'Password',
        'remember'  =>  'Remember Me',
        'forget'    =>  'Forget your Password?',
        'submit'    =>  'Login',
    ],
];