<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Layout Language Lines
    |--------------------------------------------------------------------------
    */

    'title' =>  'Officer Information Management',
    'bac'   =>  'Bangkok Aviaition Center',
    'menu'	=>	[
    	'home'	=>	'Home',
    	'officer'	=>	'Officer Management',
    	'company'	=>	'Company Management',
    	'mailgroup'	=>	'Mail Group Management',
    	'ext'	=>	'Phone & Ext. Management',
    	'mobile'	=>	'Mobile Management',
    	'setting'	=>	'Setting',
    	'logout'	=>	'Logout',
        'notice'    =>  'Officer Notice',
        'user'      =>  'User Management',
    ],
    'submenu'	=>	[
    	'officer'	=>	[
    		'officerlist'	=>	'Officer List',
    		'titlelist'	=>	'Title List',
    	],
    	'company'		=>	[
    		'departmentlist'	=>	'Department List',
    		'sectionlist'	=>	'Section List',
    		'branchlist'	=>	'Branch List',
    	],
    	'mailgroup'	=>	[
    		'list'	=>	'Mail Group List',
    	],
    	'ext'	=>	[
    		'phonelist'	=>	'Phone List',
    		'extlist'	=>	'Extension List',
    	],
    	'mobile'	=>	[
    		'list'	=>	'Mobile List',
    	],
    	'setting'	=>	[
    		'changepassword'	=>	'Change Password',
    		'changelang'	=>	'Change Language',
    	],
        'notice'    =>  [
            'list'  =>  'Notice List',
            'new'   =>  'Notice New Officer',
            'resign'    =>  'Notice Resign Officer',
        ],
    ],
    'lang'  =>  [
        'th'    =>  'ภาษาไทย',
        'en'    =>  'English',
    ],
    'button'    =>  [
        'submit'    =>  'Submit',
    ],
];