<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
   public function index()
   {
   	return view('backend.user.list');
   }

   public function getCreate()
   {
   	
   }

   public function postCreate()
   {
   	
   }

   public function getEdit()
   {
   	
   }

   public function postEdit()
   {
   	
   }

   public function delete()
   {
   	
   }
}
