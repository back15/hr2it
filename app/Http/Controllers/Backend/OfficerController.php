<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OfficerController extends Controller
{
    public function index()
    {
    	return view('backend.officer.list');
    }

    public function search()
    {
    	
    }

    public function show()
    {
    	
    }

    public function getCreate()
    {
    	
    }

    public function postCreate()
    {
    	
    }

    public function getEdit()
    {
    	
    }

    public function postEdit()
    {
    	
    }

    public function changeStatus()
    {
    	
    }
}
