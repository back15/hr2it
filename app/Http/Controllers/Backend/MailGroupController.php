<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MailGroupController extends Controller
{
    public function index()
    {
    	return view('backend.mailgroup.list');
    }

    public function search()
    {
    	
    }

    public function getCreate()
    {
    	
    }

    public function postCreate()
    {
    	
    }

    public function getEdit()
    {
    	
    }

    public function postEdit()
    {
    	
    }

    public function delete()
    {
    	
    }

    public function getAddMember()
    {
    	
    }

    public function postAddMember()
    {
    	
    }

    public function showMember()
    {
    	
    }

    public function deleteMember()
    {
    	
    }
}
