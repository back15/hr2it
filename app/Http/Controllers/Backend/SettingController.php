<?php

namespace App\Http\Controllers\Backend;

use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use App\User;

class SettingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getChangePassword()
    {
    	return view('backend.setting.changepassword');
    }

    public function postChangePassword(Request $request)
    {
    	$id = Auth::id();
        $user = User::findOrFail($id);
        $this->validate($request, ['password' => 'required|confirmed']);
        $old = $request->get('old');

        if(Hash::check($old, $user->password)) {
            $user->update(['password' => bcrypt($request->get('password'))]);
            return redirect()->route('logout');
        } else {
            return redirect()->route('backend.setting.changepassword.get');
        }
    }

    public function getChangeLang()
    {
    	return view('backend.setting.changelang');
    }

    public function postChangeLang(Request $request)
    {
    	$id = Auth::id();
        $user = User::findOrFail($id);
        $lang = $request->get('lang');

        if($lang == 0) {
            $user->update(['lang' => 'en']);
        } else {
            $user->update(['lang' => 'th']);
        }

        return redirect()->reload();
    }
}
