<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PhoneController extends Controller
{
    public function index()
    {
    	return view('backend.phone.list');
    }

    public function getCreate()
    {
    	
    }

    public function postCreate()
    {
    	
    }

    public function getEdit()
    {
    	
    }

    public function postEdit()
    {
    	
    }

    public function delete()
    {
    	
    }
}
