<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NoticeController extends Controller
{
    public function index()
    {
    	return view('backend.notice.list');
    }

    public function getNoticeNew()
    {
    	
    }

    public function postNoticeNew()
    {
    	
    }

    public function getNoticeResign()
    {
    	
    }

    public function postNoticeResign()
    {
    	
    }
}
