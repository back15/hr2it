<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Officer;
use App\Models\Department;
use App\Models\Section;
use App\Models\MailGroup;
use App\Models\Mobile;
use App\Models\Phone;

class MainController extends Controller
{
    public function searchEmail(Request $request)
    {
    	$keyword = $request->get('keyword');
    	$category = $request->get('category');
    	$officer = new Officer();

    	switch ($category) {
			case 'all':
				$data['officers'] = $officer->where('firstname_en', 'like', $keyword)
								->orWhere('firstname_th', 'like', '%' . $keyword . '%')
								->orWhere('lastname_en', 'like', $keyword)
								->orWhere('lastname_th', 'like', '%' . $keyword . '%')
								->orWhere('nickname_en', 'like', $keyword)
								->orWhere('nickname_th', 'like', '%' . $keyword . '%')
								->get();
				if (count($data['officers']) == 0) {
					$data['officers'] = $officer->join('departments', 'departments.id', '=', 'officers.department_id')
									->join('sections', 'sections.id', '=', 'officers.section_id')
									->where('departments.code', 'like', $keyword)
									->orWhere('departments.name_en', 'like', '%' . $keyword . '%')
									->orWhere('departments.name_th', 'like', '%' . $keyword . '%')
									->orWhere('sections.name_en', 'like', '%' . $keyword . '%')
									->orWhere('sections.name_th', 'like', '%' . $keyword . '%')
									->get();
				}
				break;
			
			case 'name':
				$data['officers'] = $officer->where('firstname_en', 'like', '%' . $keyword . '%')
								->orWhere('firstname_th', 'like', '%' . $keyword . '%')
								->orWhere('lastname_en', 'like', '%' . $keyword . '%')
								->orWhere('lastname_th', 'like', '%' . $keyword . '%')
								->get();
				break;

			case 'nickname':
				$data['officers'] = $officer->where('nickname_en', 'like', '%' . $keyword . '%')
								->orWhere('nickname_th', 'like', '%' . $keyword . '%')
								->get();
				break;

			case 'department':
				$data['officers'] = Department::with('officers')->get();
				break;

			case 'section':
				$data['officers'] = Section::with('officers')->get();
				break;
		}

    	return view('frontend.email', $data);
    }

    public function listMailGroup()
    {
    	return view('frontend.mailgroup');
    }

    public function showMember()
    {
    	
    }

    public function listPhone()
    {
    	return view('frontend.extension');
    }

    public function listMobile()
    {
    	return view('frontend.mobile');
    }
}
