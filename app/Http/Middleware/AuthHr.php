<?php

namespace App\Http\Middleware;

use Closure;

class AuthHr
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()->user_role != 2) {
            return redirect()->guest('');
        }
        return $next($request);
    }
}
