<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $table = 'users';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'password', 'user_role_id', 'officer_id', 'lang'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getRole() 
    {
        return $this->belongsTo('App\Models\UserRole', 'user_role_id');
    }

    public function getOfficer() 
    {
        return $this->belongsTo('App\Models\Officer', 'officer_id');
    }

    public function userLogs() 
    {
        return $this->hasMany(UserLog::class);
    }

    public function loginLogs() 
    {
        return $this->hasMany(LoginLog::class);
    }

    public function noticeNews() 
    {
        return $this->hasMany(NoticeNewOfficer::class);
    }

    public function noticeResigns() 
    {
        return $this->hasMany(NoticeResignOfficer::class);
    }
}
