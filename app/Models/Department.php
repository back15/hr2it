<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['code', 'name_en', 'name_th', 'section_id'];

    public function getSection() 
    {
    	return $this->belongsTo('App\Models\Section', 'section_id');
    }

    public function officers() 
    {
    	return $this->hasMany(Officer::class);
    }
}
