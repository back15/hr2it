<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name_en', 'name_th'];

    public function officers() 
    {
    	return $this->hasMany(Officer::class);
    }

    public function phones() 
    {
    	return $this->hasMany(Phone::class);
    }
}
