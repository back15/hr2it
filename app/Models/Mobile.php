<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mobile extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['number', 'department_id', 'remark_en', 'remark_th'];

    public function getDepartment() 
    {
    	return $this->belongsTo('App\Models\Department', 'department_id');
    }
}
