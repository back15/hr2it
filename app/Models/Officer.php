<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Officer extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'officer_id', 'title_id', 'firstname_en', 'firstname_th', 'lastname_en', 'lastname_th',
    	'nickname_en', 'nickname_th', 'position_en', 'position_th', 'department_id', 'section_id',
    	'branch_id', 'email', 'mobile', 'officer_status_id', 'officer_type_id', 'startdate', 'enddate'
    ];

    public function getTitle() 
    {
    	return $this->belongsTo('App\Models\Title', 'title_id');
    }

    public function getDepartment() 
    {
    	return $this->belongsTo('App\Models\Department', 'department_id');
    }

    public function getSection() 
    {
    	return $this->belongsTo('App\Models\Section', 'section_id');
    }

    public function getBranch() 
    {
    	return $this->belongsTo('App\Models\Branch', 'branch_id');
    }

    public function getStatus() 
    {
    	return $this->belongsTo('App\Models\OfficerStatus', 'officer_status_id');
    }

    public function getType() 
    {
    	return $this->belongsTo('App\Models\OfficerType', 'officer_type_id');
    }

    public function mailGroups() 
    {
    	return $this->belongsToMany('App\Models\MailGroup');
    }
}
