<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name_en', 'name_th'];

    public function departments() 
    {
    	return $this->hasMany(Department::class);
    }

    public function officers() 
    {
        return $this->hasMany(Officer::class);
    }
}
