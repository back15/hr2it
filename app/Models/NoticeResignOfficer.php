<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NoticeResignOfficer extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['officer_id', 'user_id', 'date'];

    public function getOfficer() 
    {
    	return $this->belongsTo('App\Models\Officer', 'officer_id');
    }

    public function getUser() 
    {
    	return $this->belongsTo('App\User', 'user_id');
    }
}
