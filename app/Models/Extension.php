<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Extension extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['number', 'department_id', 'phone_id', 'remark_en', 'remark_th'];

    public function getDepartment() 
    {
    	return $this->belongsTo('App\Models\Department', 'department_id');
    }

    public function getPhone() 
    {
    	return $this->belongsTo('App\Models\Phone', 'phone_id');
    }
}
