<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Phone extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['number', 'branch_id', 'remark_en', 'remark_th'];

    public function getBranch() 
    {
    	return $this->belongsTo('App\Models\Branch', 'branch_id');
    }

    public function extensions() 
    {
    	return $this->hasMany(Extension::class);
    }
}
