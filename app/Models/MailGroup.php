<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MailGroup extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['email', 'department_id', 'remark_en', 'remark_th'];

    public function getDepartment() 
    {
    	return $this->belongsTo('App\Models\Department', 'department_id');
    }

    public function officers() 
    {
    	return $this->belongsToMany('App\Models\Officer');
    }
}
