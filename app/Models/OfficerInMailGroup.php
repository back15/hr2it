<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OfficerInMailGroup extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['officer_id', 'mail_group_id'];

    public function getOfficer() 
    {
    	return $this->belongsTo('App\Models\Officer', 'officer_id');
    }

    public function getMailGroup() 
    {
    	return $this->belongsTo('App\Models\MailGroup', 'mail_group_id');
    }
}
