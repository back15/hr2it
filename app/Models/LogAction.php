<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LogAction extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name_en', 'name_th'];

    public function userLogs() 
    {
    	return $this->hasMany(UserLog::class);
    }
}
