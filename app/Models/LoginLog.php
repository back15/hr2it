<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LoginLog extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'ip'];

    public function getUser() 
    {
    	return $this->belongsTo('App\User', 'user_id');
    }
}
