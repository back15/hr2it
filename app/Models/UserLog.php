<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserLog extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'action_id', 'data'];

    public function getUser() 
    {
    	return $this->belongsTo('App\User', 'user_id');
    }

    public function getAction() 
    {
    	return $this->belongsTo('App\Models\LogAction', 'action_id');
    }
}
